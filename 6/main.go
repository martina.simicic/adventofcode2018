package main

import (
  "sort"
  "bufio"
  "fmt"
  "log"
  "os"
  "regexp"
  "strconv"
)

type Coordinate struct {
  x int
  y int
  original bool
}

func main() {
  file_location := "/home/precious/adventOfCode2018/6/input.txt"
  // file_location := "/home/precious/adventOfCode2018/6/input_test.txt"

  CalculateManhattanDistance(file_location)
}

func CalculateManhattanDistance(file_location string) {
  var sorted_lines []string

  file, err := os.Open(file_location)
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    sorted_lines = append(sorted_lines, scanner.Text())
  }

  sort.Strings(sorted_lines)

  var minx, miny, maxx, maxy int
  coordinates := []Coordinate{}
  for _, line := range sorted_lines {
    x_y := regexp.MustCompile(", ").Split(line, 2)
    x, _ := strconv.Atoi(x_y[0])c
    y, _ := strconv.Atoi(x_y[1])

    coordinate := Coordinate{x: x, y: y, original: true}
    coordinates = append(coordinates, coordinate)

    if
  }



  for _, coo := range coordinates {
    fmt.Println(coo)
  }
}
