package main

import "testing"

func TestSumOfMetadataEntries(t *testing.T) {
	input := "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"
	got := SumOfMetadataEntries(input)
	expected := 138

	if got != expected {
		t.Error("Fout, expecting: ", expected, " but got: ", got)
	}
}
