package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
)

func main() {
	// file_location := "/home/precious/adventOfCode2018/8/input.txt"
	file_location := "/home/precious/adventOfCode2018/8/input_test.txt"

	string_from_file := StringFromFile(file_location)
	SumOfMetadataEntries(string_from_file)
}

func SumOfMetadataEntries(string_from_file string) int {
	entries := []int{}
	for _, entry := range regexp.MustCompile(" ").Split(string_from_file, -1) {
		converted, _ := strconv.Atoi(entry)
		entries = append(entries, converted)
	}

	fmt.Println(entries)

	sum_metadata := 0
	metadata := Metadata(entries, &sum_metadata)

	fmt.Println(metadata)
	return 10
}

func Metadata(elements []int, sum *int) int {
	fmt.Println("Running check for: ", elements)
	if len(elements) < 0 {
		return 0
	}

	if elements[0] == 0 {
		number_of_metadata := elements[1]
		for _, val := range elements[1:number_of_metadata] {
			*sum += val
		}

		Metadata(elements[2+number_of_metadata:], sum)
	} else {
		Metadata(elements[2:], sum)
	}

	return 10
}

func StringFromFile(file_location string) string {
	b, err := ioutil.ReadFile(file_location)
	if err != nil {
		fmt.Print(err)
	}

	string_from_file := string(b)

	return string_from_file
}
