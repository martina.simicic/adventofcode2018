package main

import "testing"

func TestDiffLetters(t *testing.T) {
  input := []string{
    "abcde",
    "fghij",
    "klmno",
    "pqrst",
    "fguij",
    "axcye",
    "wvxyz",
  }

  expected := 1
  got := DiffLetters(input)

  if got != expected {
    t.Error("Error! Difference was: ", got)
  }
}

func TestNumberOfDiffLetters (t *testing.T){
  word1 := "fghij"
  word2 := "fguij"

  expected := 1
  got := NumberOfDiffLetters(word1, word2)

  if got != expected {
    t.Error("Error! Difference was: ", got)
  }
}
