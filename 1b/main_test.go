package main

import "testing"

var allTests = []struct {
        input    []int // input
        expected int   // expected result
}{
        {[]int{1, -1}, 0},
        {[]int{3, 3, 4, -2, -4}, 10},
        {[]int{-6, 3, 8, 5, -6}, 5},
        {[]int{7, 7, -2, -7, -4}, 14},
}

func TestRepeatedSum(t *testing.T) {
  for _, val := range allTests {
    got := RepeatedSum(val.input)

    if got != val.expected {
      t.Error("Fout, it was: ", got)
    }
  }
}
