package main

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Velocity struct {
	vel_x int
	vel_y int
}

type DataPoint struct {
	point    image.Point
	velocity Velocity
}

const scale int = 1

func main() {
	// file_location := "/home/precious/adventOfCode2018/10/input_test.txt"
	file_location := "/home/precious/adventOfCode2018/10/input.txt"

	Draw(file_location)
}

func Draw(file_location string) {
	lines := []string{}

	file, err := os.Open(file_location)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	sort.Strings(lines)

	points := []DataPoint{}

	min_x := 10
	min_y := 10
	max_x := -10
	max_y := -10

	line_data_reg := regexp.MustCompile(`position=<(.+), (.+)> velocity=<(.+), (.+)>`)
	for _, line := range lines {
		line_data := line_data_reg.FindStringSubmatch(line)

		x, _ := strconv.Atoi(strings.Trim(line_data[1], " "))
		y, _ := strconv.Atoi(strings.Trim(line_data[2], " "))
		vel_x, _ := strconv.Atoi(strings.Trim(line_data[3], " "))
		vel_y, _ := strconv.Atoi(strings.Trim(line_data[4], " "))

		if x < min_x {
			min_x = x
		} else if x > max_x {
			max_x = x
		}

		if y < min_y {
			min_y = y
		} else if y > max_y {
			max_y = y
		}

		point := DataPoint{
			point:    image.Point{X: x, Y: y},
			velocity: Velocity{vel_x: vel_x, vel_y: vel_y},
		}

		points = append(points, point)
	}

	fmt.Println("Min: ", min_x, min_y)
	fmt.Println("Max: ", max_x, max_y)

	after_iteration := points

	min_point := image.Point{X: min_x, Y: min_y}
	max_point := image.Point{X: max_x, Y: max_y}
	for i := 0; i < 11000; i++ {
		after_iteration = DrawingLoop(after_iteration, min_point, max_point, i)
		min_point, max_point = MinMaxPoints(after_iteration)
		// bufio.NewReader(os.Stdin).ReadBytes('\n')
	}
}

func DrawingLoop(elements []DataPoint, min_point image.Point, max_point image.Point, i int) []DataPoint {
	after_iteration := []DataPoint{}

	if i > 10000 && i < 10200 {
		DrawImage(elements, min_point, max_point, i)
	}

	for _, v := range elements {
		point_after := DataPoint{
			point:    image.Point{X: v.point.X + v.velocity.vel_x, Y: v.point.Y + v.velocity.vel_y},
			velocity: Velocity{vel_x: v.velocity.vel_x, vel_y: v.velocity.vel_y},
		}
		after_iteration = append(after_iteration, point_after)
	}

	fmt.Println("Done")

	return after_iteration
}

func DrawImage(elements []DataPoint, min_point image.Point, max_point image.Point, i int) {
	pic := image.NewGray(image.Rectangle{Min: min_point.Div(scale), Max: max_point.Div(scale)})

	for _, v := range elements {
		pic.Set(v.point.X/scale, v.point.Y/scale, color.White)
	}

	f, err := os.Create("/home/precious/test_images/image" + strconv.Itoa(i) + ".png")
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, pic); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}

func MinMaxPoints(entries []DataPoint) (image.Point, image.Point) {
	min_x := 10
	min_y := 10
	max_x := -10
	max_y := -10

	for _, entry := range entries {
		if entry.point.X < min_x {
			min_x = entry.point.X
		} else if entry.point.X > max_x {
			max_x = entry.point.X
		}

		if entry.point.Y < min_y {
			min_y = entry.point.Y
		} else if entry.point.Y > max_y {
			max_y = entry.point.Y
		}
	}

	min_point := image.Point{X: min_x, Y: min_y}
	max_point := image.Point{X: max_x, Y: max_y}

	return min_point, max_point
}
