package main

import "testing"

func TestCalculateChecksum(t *testing.T) {
  input := []string{
    "abcdef",
    "bababc",
    "abbcde",
    "abcccd",
    "aabcdd",
    "abcdee",
    "ababab",
  }

  expected := 12
  got := CalculateChecksum(input)

  if got != expected {
    t.Error("Error! Checksum was: ", got)
  }
}
