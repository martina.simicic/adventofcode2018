package main

import (
	"fmt"
	"image"
	"math"
)

func main() {
	serial_number := 1309
	fmt.Println(PowerCalculation(serial_number))
}

func PowerCalculation(serial_number int) (image.Point, int) {
	power_grid := make(map[image.Point]int)

	for i := 1; i <= 300; i++ {
		for j := 1; j <= 300; j++ {
			cell := image.Point{i, j}
			power_grid[cell] = CellCalculation(cell, serial_number)
		}
	}

	strongest_corner_cell, strenght := DetermineStrongesCellGroup(power_grid)
	return strongest_corner_cell, strenght
}

func DetermineStrongesCellGroup(power_grid map[image.Point]int) (image.Point, int) {
	max_power := -10000
	max_power_cell := image.Point{}
	max_cell_size := 3

	cell_count := 0
	for cell, _ := range power_grid {
		for k := 3; k < 297; k++ {
			group_power := 0

			for i := cell.X; i < cell.X+k; i++ {
				for j := cell.Y; j < cell.Y+k; j++ {
					if i > 300 || j > 300 {
						break
					}
					group_power += power_grid[image.Point{i, j}]
				}
			}

			if group_power > max_power {
				max_power = group_power
				max_power_cell = cell
				max_cell_size = k
			}
		}
		cell_count++
		fmt.Println(cell_count)
	}

	fmt.Println(max_power_cell, max_power, max_cell_size)
	return max_power_cell, max_power
}

func CellCalculation(cell image.Point, serial_number int) int {
	cell_rack_id := cell.X + 10

	power := cell_rack_id*cell.Y + serial_number
	power = power * cell_rack_id
	power = Digit(power, 3)
	power = power - 5

	return power
}

func Digit(num, place int) int {
	r := num % int(math.Pow(10, float64(place)))
	return r / int(math.Pow(10, float64(place-1)))
}
