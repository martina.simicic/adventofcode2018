package main

import (
	"image"
	"testing"
)

var allTests = []struct {
	corner_data_cell image.Point // input
	serial_number    int         //input
	power_level      int         // expected result
}{
	{image.Point{33, 45}, 18, 29},
	// {image.Point{21, 61}, 42, 30},
	// {image.Point{122, 79}, 57, -5},
	// {image.Point{217, 196}, 39, 0},
	// {image.Point{101, 153}, 71, 4},
}

func TestPowerCalculation(t *testing.T) {
	for _, val := range allTests {
		got_corner_data_cell, got_power_level := PowerCalculation(val.serial_number)

		if got_corner_data_cell != val.corner_data_cell && got_power_level != val.power_level {
			t.Error("Error, data cell: ", val.corner_data_cell, " got: ", got_corner_data_cell)
			t.Error("Error, power level : ", val.power_level, " got: ", got_power_level)
		}
	}
}

func TestCellCalculation(t *testing.T) {
	got := CellCalculation(image.Point{3, 5}, 8)
	expected := 4

	if got != expected {
		t.Error("Power level: ", got, " expected: ", expected)
	}
}
