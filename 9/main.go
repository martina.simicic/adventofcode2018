package main

import (
	"fmt"
)

func main() {
	// 479 players; last marble is worth 71035 points
	number_of_players := 479
	marble_count := 7103500

	highest_score := GamePlay(number_of_players, marble_count)
	fmt.Println(highest_score)
}

func GamePlay(number_of_players int, marble_count int) int {
	marble_circle := []int{0, 2, 1, 3}
	scores := make(map[int]int)

	lenght_of_circle := len(marble_circle)
	current_player := 3
	previous_position := 3

	for current_marble := 4; current_marble <= marble_count; current_marble++ {
		fmt.Print("-", current_marble, "-")

		if current_player == number_of_players {
			current_player = 1
		} else {
			current_player += 1
		}

		// fmt.Println("Player ", current_player, " plays with marble: ", current_marble)
		// previous_position := FindPreviousPosition(previous_element, marble_circle)

		if current_marble%23 == 0 {
			scores[current_player] += current_marble
			// find marble on the 7 positions before

			// fmt.Println("Prev - 7: ", previous_position-7)
			// special_position := int(math.Abs(float64(previous_position) - 7))
			// fmt.Println("Special pos: ", special_position)

			special_position := previous_position - 7
			if special_position < 0 {
				special_position = special_position + lenght_of_circle
			}

			scores[current_player] += marble_circle[special_position]
			marble_circle = append(marble_circle[:special_position], marble_circle[special_position+1:]...)

			previous_position = special_position
		} else {
			// Make space in the array for a new element. You can assign it any value.
			marble_circle = append(marble_circle, 0)

			p_before := (previous_position + 1) % lenght_of_circle
			p_after := (previous_position + 2) % lenght_of_circle

			if p_after == 0 {
				p_after = lenght_of_circle
			}
			copy(marble_circle[p_after:], marble_circle[p_after-1:])

			marble_circle[p_before+1] = current_marble
			previous_position = p_before + 1
		}

		lenght_of_circle = len(marble_circle)

		// fmt.Println("State of circle after: ", marble_circle)
		// fmt.Println("------------")
	}
	max_score := 0
	for _, val := range scores {
		if val > max_score {
			max_score = val
		}
	}

	// fmt.Println(scores)
	return max_score
}

// func FindPreviousPosition(previous_element int, marble_circle []int) int {
// 	index := 1
//
// 	for i, v := range marble_circle {
// 		if v == previous_element {
// 			index = i
// 		}
// 	}
//
// 	return index
// }
