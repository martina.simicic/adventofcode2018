package main

import "testing"

var allTests = []struct {
	number_of_players int // input
	marble_count      int //input
	highest_score     int // expected result
}{
	// {9, 25, 32},
	// {10, 1618, 8317},
	// {13, 7999, 146373},
	// {17, 1104, 2764},
	// {21, 6111, 54718},
	// {30, 5807, 37305},
	// {479, 20, 20},
	{479, 2, 20},
}

func TestGamePlay(t *testing.T) {
	for _, val := range allTests {
		got := GamePlay(val.number_of_players, val.marble_count)

		if got != val.highest_score {
			t.Error("Fout, expecting: ", val.highest_score, " but got: ", got)
		}
	}
}
