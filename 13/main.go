package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type Coordinate struct {
	x int
	y int
}

type Cart struct {
	position     Coordinate
	old_position Coordinate
	direction    string
	tick         int
}

func main() {
	// file_location := "/home/precious/adventOfCode2018/13/input_test_2.txt"
	// file_location := "/home/precious/adventOfCode2018/13/input_test.txt"
	// file_location := "/home/precious/adventOfCode2018/13/input.txt"
	file_location := "/home/precious/adventOfCode2018/13/input_thijs.txt"

	area_map, carts := ImportMap(file_location)

	collision_coordinate := FindCollision(area_map, carts)
	fmt.Println(collision_coordinate)
}

func FindCollision(area_map map[Coordinate]string, carts []Cart) Coordinate {
	collision_coordinate := Coordinate{-10, -10}
	i := 1

	for {
		fmt.Println("Carts before (", len(carts), "): ", carts)
		if len(carts) < 2 {
			collision_coordinate = carts[0].position
			break
		}

		for cart_index, cart := range carts {
			move := ""
			new_position := Coordinate{}
			switch cart.direction {
			case "up":
				new_position = Coordinate{cart.position.x, cart.position.y - 1}
			case "down":
				new_position = Coordinate{cart.position.x, cart.position.y + 1}
			case "left":
				new_position = Coordinate{cart.position.x - 1, cart.position.y}
			case "right":
				new_position = Coordinate{cart.position.x + 1, cart.position.y}
			}
			cart.old_position = cart.position
			cart.position = new_position
			move = area_map[cart.position]

			fmt.Println("For cart: ", cart, " move is: ", move)
			switch move {
			case "/":
				// move down left or up rigt
				if cart.direction == "up" {
					cart.direction = "right"
				} else if cart.direction == "down" {
					cart.direction = "left"
				} else if cart.direction == "left" {
					cart.direction = "down"
				} else {
					cart.direction = "up"
				}
			case "\\":
				// move down right or up left
				if cart.direction == "up" {
					cart.direction = "left"
				} else if cart.direction == "down" {
					cart.direction = "right"
				} else if cart.direction == "left" {
					cart.direction = "up"
				} else {
					cart.direction = "down"
				}
			case "+":
				switch cart.tick {
				case 1:
					// move left
					switch cart.direction {
					case "right":
						cart.direction = "up"
					case "left":
						cart.direction = "down"
					case "up":
						cart.direction = "left"
					case "down":
						cart.direction = "right"
					}
				case 3:
					//move right
					switch cart.direction {
					case "right":
						cart.direction = "down"
					case "left":
						cart.direction = "up"
					case "up":
						cart.direction = "right"
					case "down":
						cart.direction = "left"
					}
				}
				cart.tick = (cart.tick % 3) + 1
			}
			fmt.Println("Cart values after: ", cart)
			carts[cart_index] = cart
		}
		// fmt.Println("Carts after", carts)
		encountered := make(map[Coordinate]bool)
		collision_coordinates := []Coordinate{}

		for i, cart_data := range carts {
			if encountered[carts[i].position] == true {
				collision_coordinate = cart_data.position
				collision_coordinates = append(collision_coordinates, collision_coordinate)
			} else {
				encountered[carts[i].position] = true
			}
		}

		for _, co := range collision_coordinates {
			carts_after_cleanup := []Cart{}
			for i, cart := range carts {
				if cart.position != co {
					fmt.Println(i, len(carts))
					carts_after_cleanup = append(carts_after_cleanup, cart)
				}
			}
			carts = carts_after_cleanup
		}

		carts_after_direct_collision_cleanup := []Cart{}
		for _, cart1 := range carts {
			keep := true
			for _, cart2 := range carts {
				if cart1.old_position == cart2.position && cart1.position == cart2.old_position {
					keep = false
				}
			}

			if keep == true {
				carts_after_direct_collision_cleanup = append(carts_after_direct_collision_cleanup, cart1)
			}
		}
		carts = carts_after_direct_collision_cleanup

		fmt.Println("After the iteration: ", len(carts))
		fmt.Println("===============================")
		// first part of the exercise
		// default_value := Coordinate{-10, -10}
		// if collision_coordinate != default_value {
		// 	break
		// } else {
		// 	fmt.Println(i)
		// }
		i++
	}

	return collision_coordinate
}

func ImportMap(file_location string) (map[Coordinate]string, []Cart) {
	area_map := make(map[Coordinate]string)
	carts := []Cart{}

	file, err := os.Open(file_location)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	y := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "")

		for x, value := range line {
			switch value {
			case "^":
				cart := Cart{position: Coordinate{x, y}, direction: "up", tick: 1}
				carts = append(carts, cart)
				value = "|"
			case "v":
				cart := Cart{position: Coordinate{x, y}, direction: "down", tick: 1}
				carts = append(carts, cart)
				value = "|"
			case ">":
				cart := Cart{position: Coordinate{x, y}, direction: "right", tick: 1}
				carts = append(carts, cart)
				value = "-"
			case "<":
				cart := Cart{position: Coordinate{x, y}, direction: "left", tick: 1}
				carts = append(carts, cart)
				value = "-"
			}

			area_map[Coordinate{x, y}] = value
		}

		y++
	}

	return area_map, carts
}
