package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
)

func main() {
	file_location := "/home/precious/adventOfCode2018/7/input.txt"
	// file_location := "/home/precious/adventOfCode2018/7/input_test.txt"

	BuildDependencies(file_location)
}

func BuildDependencies(file_location string) {
	lines := []string{}

	file, err := os.Open(file_location)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	dependencies := make(map[string][]string)
	actions_steps := []string{}
	nodes_regex := regexp.MustCompile(`Step (.) must be finished before step (.) can begin.`)
	for _, line := range lines {
		nodes := nodes_regex.FindStringSubmatch(line)
		dependencies[nodes[2]] = append(dependencies[nodes[2]], nodes[1])
		if Contains(actions_steps, nodes[1]) == false {
			actions_steps = append(actions_steps, nodes[1])
		}

		if Contains(actions_steps, nodes[2]) == false {
			actions_steps = append(actions_steps, nodes[2])
		}
	}

	sort.Strings(actions_steps)

	// fmt.Println(dependencies)
	// fmt.Println(actions_steps)
	order_of_execution := []string{}
	actions_processed := 0
	total_action_count := len(actions_steps)

	for {
		if actions_processed == total_action_count {
			break
		}

		for action_i, action := range actions_steps {
			has_dependency := false

			for key, dependency := range dependencies {
				if key == action && len(dependency) > 0 {
					has_dependency = true
				}
			}

			if has_dependency == false {
				order_of_execution = append(order_of_execution, action)

				fmt.Println("Order of execution: ", order_of_execution)
				actions_processed++
				actions_steps = append(actions_steps[:action_i], actions_steps[action_i+1:]...)

				for key, dependency := range dependencies {
					for i, dep := range dependency {
						if dep == action {
							dependencies[key] = append(dependency[:i], dependency[i+1:]...)
						}
					}
				}

				break
			} else {
				fmt.Println("Skipped action", action, ", depe: ", dependencies[action])
			}
		}
	}
}

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
