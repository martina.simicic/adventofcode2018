package main

import (
  "fmt"
  "io/ioutil"
  "sync"
  "strings"
)

var wg sync.WaitGroup

func main() {
  file_location := "/home/precious/adventOfCode2018/5/input.txt"
  // file_location := "/home/precious/adventOfCode2018/5/test_input.txt"

  start_polymer := StringFromFile(file_location)
  occuring_letters := Polymerize(start_polymer)

  // fmt.Println(occuring_letters)

  for letter, _ := range occuring_letters {
    string_after_replacement := strings.Replace(start_polymer, letter, "", -1)
    string_after_replacement = strings.Replace(string_after_replacement, strings.Title(letter), "", -1)
    occuring_letters[letter] = string_after_replacement
  }

  for _, input := range occuring_letters {
    Polymerize(input)
  }

  // fmt.Println(occuring_letters)

  // fmt.Println(leftover_polymer)
  // fmt.Println(len(leftover_polymer))
}

func StringFromFile(file_location string) string {
  b, err := ioutil.ReadFile(file_location)
  if err != nil {
    fmt.Print(err)
  }

  string_from_file := string(b)

  return string_from_file
}

func Polymerize(start_polymer string) map[string]string {
  changes_in_the_round_made := false
  polymer_after_iteration := start_polymer

  letters_and_lenght_without := make(map[string]string)

  // fmt.Println("Before: ", len(polymer_after_iteration))
  for i, letter := range start_polymer {
    if i + 1 < len(start_polymer) {
      next_letter := start_polymer[i+1]
      difference := letter - rune(next_letter)

      if difference == 32 || difference == -32  {
        changes_in_the_round_made = true

        if difference == 32 {
          letters_and_lenght_without[string(letter)] = ""
        } else {
          letters_and_lenght_without[string(next_letter)] = ""
        }

        string_to_replace := string(letter) + string(next_letter)
        // fmt.Println("Before:", polymer_after_iteration)
        polymer_after_iteration = strings.Replace(polymer_after_iteration, string_to_replace, "", -1)
        // fmt.Println("After:", polymer_after_iteration)
      }
    }
  }

  // fmt.Println("After: ", len(polymer_after_iteration))

  if changes_in_the_round_made == true {
    Polymerize(polymer_after_iteration)
  } else {
    if len(polymer_after_iteration) == 5331 {
      fmt.Println("Final number of characters: ", len(polymer_after_iteration))
      fmt.Println("Final string: ", polymer_after_iteration)
    }
    fmt.Println("Final number of characters: ", len(polymer_after_iteration))
    // fmt.Println("Final string: ", polymer_after_iteration)
  }

  // fmt.Println(letters_and_lenght_without)

  return letters_and_lenght_without
}
