package main

import "testing"

func TestCountOfSafePatches(t *testing.T) {
  file_location := "/home/precious/adventOfCode2018/3a/test_input.txt"

  expected := 4
  got := CountOfSafePatches(file_location)

  if got != expected {
    t.Error("Error! Safe patches were: ", got)
  }
}
