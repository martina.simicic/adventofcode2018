package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "regexp"
  "strings"
  "strconv"
)

type Coordinate struct {
  x int
  y int
}

type Patch struct {
  id string
  is_overlapping bool
}

func main() {
  file_location := "/home/precious/adventOfCode2018/3a/input.txt"

  fmt.Println(CountOfSafePatches(file_location))
}

func CountOfSafePatches(file_location string) int {
  patches_data := make(map[Coordinate]int)
  patches := make(map[Coordinate]*Patch)

  file, err := os.Open(file_location)
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    // fmt.Println(scanner.Text())
    line_data := regexp.MustCompile(" ").Split(scanner.Text(), 4)

    // line_data
    // #1 @ 335,861: 14x10
    // 0 - id
    // 1 - nothing
    // 2 - offsets (with :)
    // 3 - size of rectangle

    offsets := strings.Replace(line_data[2], ":", "", -1)
    offsets_data := regexp.MustCompile(",").Split(offsets, 2)

    offset_x, _ := strconv.Atoi(offsets_data[0])
    offset_y, _ := strconv.Atoi(offsets_data[1])

    // fmt.Println(offset_x, offset_y)

    rectangle_data := regexp.MustCompile("x").Split(line_data[3], 2)

    rectangle_width, _  := strconv.Atoi(rectangle_data[0])
    rectangle_height, _ := strconv.Atoi(rectangle_data[1])

    // fmt.Println(rectangle_width, rectangle_height)
    patch := Patch{id: line_data[0]}

    // log the rectangle starting form the beginning coordinate
    for i := 0; i < rectangle_width; i++ {
      for j := 0; j < rectangle_height; j++ {
        coordinate := Coordinate{x: offset_x + i, y: offset_y + j}
        patches_data[coordinate]++

        if patches_data[coordinate] >= 2 {
          patch.is_overlapping = true
          patches[coordinate].is_overlapping = true
        }

        patches[coordinate] = &patch
      }
    }
  }

  counter := 0
  for _, value := range patches_data {
    if value >= 2 {
      counter++
    }
  }

  fmt.Println(counter)

  for _, v := range patches {
    if v.is_overlapping == false {
      fmt.Println(v)
    }
  }
  if err := scanner.Err(); err != nil {
    log.Fatal(err)
  }

  return counter
}
