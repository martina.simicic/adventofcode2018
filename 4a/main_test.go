package main

import "testing"

func TestParseEvents(t *testing.T) {
  file_location := "/home/precious/adventOfCode2018/4a/test_input.txt"

  expected := 24
  got := ParseEvents(file_location)

  if got != expected {
    t.Error("Error! Return was ", got)
  }
}
