package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "sort"
  "regexp"
  "strconv"
)

type Event struct {
  month int
  day int
  minute int
  guard_id string
  action string
}

type MinuteTimes struct {
  minute int
  times int
}

func main() {
  file_location := "/home/precious/adventOfCode2018/4a/input.txt"

  ParseEvents(file_location)
}

// parse the date, format: [year-month-day hour:minute]
// sort the input
// iterate and store when a guard was asleep during an hour
// iterate to see which guard slept the most
// for the sleepiest guard find the minute

func ParseEvents(file_location string) int {
  var sorted_lines []string
  var events []Event

  file, err := os.Open(file_location)
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    sorted_lines = append(sorted_lines, scanner.Text())
  }

  sort.Strings(sorted_lines)

  action_re := regexp.MustCompile("(Guard #[0-9]+)|(wakes up)|(falls asleep)")
  day_re := regexp.MustCompile(`1518-(..)-(..)`)
  minute_re := regexp.MustCompile(`:(..)`)

  for _, line := range sorted_lines {
    action := action_re.FindAllString(line, 1)
    // _ := day_re.FindAllString(line, 2)[0]
    day_and_minute := day_re.FindStringSubmatch(line)
    month := day_and_minute[1]
    day := day_and_minute[2]
    minute := minute_re.FindStringSubmatch(line)

    m_n, _ := strconv.Atoi(month)
    d_n, _ := strconv.Atoi(day)
    min_n, _ := strconv.Atoi(minute[1])

    event := Event{
      month: m_n,
      day: d_n,
      minute: min_n,
      action: fmt.Sprint(action[0]),
    }

    events = append(events, event)
  }

  day_minute_asleep := make(map[int]int)
  day_minute_guard := make(map[string]int)
  day_minute_guard_when := make(map[string][]int)
  var guards []string
  current_guard := ""

  for i, item := range events {
    if item.action == "falls asleep" {
      // increase for until the next event, log guard
      // fmt.Println("Processing: ", item)
      // fmt.Println("Next event: ", events[i+1])
      for minute := item.minute; minute < events[i+1].minute; minute++ {
        day_minute_asleep[minute] += 1
        // fmt.Println("Adding for minute: ", minute)
        day_minute_guard[current_guard] += 1
        day_minute_guard_when[current_guard] = append(day_minute_guard_when[current_guard], minute)
      }
    } else if item.action == "wakes up" {
      // nothing
    } else {
      current_guard = item.action
      guards = append(guards, current_guard)
    }
  }

  max := 0
  max_i := ""
  for i, v := range day_minute_guard {
    if v > max {
      max = v
      max_i = i
    }
  }

  fmt.Println("Sleepiest guard: ", max_i, " slept: ", day_minute_guard[max_i])
  fmt.Println("Sleeping times of a guard: ", day_minute_guard_when[max_i])

  // map minute and occurence
  sleepy_occurence := make(map[int]int)
  for _, minute_val := range day_minute_guard_when[max_i] {
    sleepy_occurence[minute_val] += 1
  }

  mmax := 0
  mmax_i := 0
  for i, v := range sleepy_occurence {
    if v > mmax {
      mmax = v
      mmax_i = i
    }
  }

  fmt.Println("From all minute: ", mmax_i, " times: ", sleepy_occurence[mmax_i])

  // Part 2
  fmt.Println(guards)
  sleepiest := make(map[string]MinuteTimes)

  for _, guard_val := range guards {
    sleepy_occurence_guard := make(map[int]int)

    for _, minute_val := range day_minute_guard_when[guard_val] {
      sleepy_occurence_guard[minute_val] += 1
    }

    g_mmax := 0
    g_mmax_i := 0
    for i, v := range sleepy_occurence_guard {
      if v > g_mmax {
        g_mmax = v
        g_mmax_i = i
      }
    }

    minute_times := MinuteTimes{minute: g_mmax_i, times: g_mmax }
    sleepiest[guard_val] = minute_times
  }

  fmt.Println("Sleepiest: ", sleepiest)

  return 10
}
