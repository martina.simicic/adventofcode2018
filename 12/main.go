package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

type Pot struct {
	position int
	value    string
}

func main() {
	// file_location := "/home/precious/adventOfCode2018/12/input_test.txt"
	file_location := "/home/precious/adventOfCode2018/12/input.txt"

	//test
	// initial_state := "#..#.#..##......###...###"
	//assignment
	initial_state := "#...#..##.......####.#..###..#.##..########.#.#...#.#...###.#..###.###.#.#..#...#.#..##..#######.##"

	rules := GetRules(file_location)
	pots := CreatePots(initial_state)
	PredictFuture(pots, rules)
}

func PredictFuture(pots []Pot, rules map[string]string) {
	// original_lenght := len(pots)
	for i := 0; i < 100; i++ {
		// add empty buckets on left and right
		first_plant_position := len(pots)
		first_plant_index := len(pots)
		last_plant_position := 0
		last_plant_index := 0

		for j, pot := range pots {
			if pot.value == "#" && j < first_plant_index {
				first_plant_index = j
				first_plant_position = pot.position
			}
			if pot.value == "#" && j > last_plant_index {
				last_plant_index = j
				last_plant_position = pot.position
			}
		}
		empty_pots := []Pot{}
		for i := -4; i <= 4; i++ {
			if i == 0 {
				continue
			}
			position := i
			if i < 0 {
				position = i + first_plant_position
			} else if i > 0 {
				position = i + last_plant_position
			}
			empty_pots = append(empty_pots, Pot{value: ".", position: position})
		}

		// fmt.Println("Before appending: ", StringForPots(pots))
		pots = append(empty_pots[0:4], pots[first_plant_index:last_plant_index+1]...)
		pots = append(pots, empty_pots[4:]...)
		// fmt.Println("After appending : ", StringForPots(pots))
		// fmt.Println(i, ": ", StringForPots(pots))
		pots = SimulateYear(pots, rules)

		fmt.Println(i, ": ", StringForPots(pots))
	}
	fmt.Println(pots)
	sum_pots := 0
	for _, pot := range pots {
		// fmt.Println("val: ", val, "current count: ", count)
		if pot.value == "#" {
			sum_pots += pot.position
		}
	}

	fmt.Println(sum_pots)
}

func SimulateYear(pots []Pot, rules map[string]string) []Pot {
	next_year_state := make([]Pot, len(pots))
	//copy(pots, next_year_state)

	next_year_state[0] = pots[0]
	next_year_state[1] = pots[1]
	next_year_state[len(pots)-2] = pots[len(pots)-2]
	next_year_state[len(pots)-1] = pots[len(pots)-1]

	for i := 2; i < len(pots)-2; i++ {
		to_check := StringForPots(pots[i-2 : i+3])

		matching_rule := rules[to_check]
		if matching_rule == "" {
			matching_rule = "."
		}

		// fmt.Println("I: ", i, "Ch: ", to_check, " match: ", matching_rule)
		new_pot := Pot{position: pots[i].position, value: matching_rule}
		next_year_state[i] = new_pot
	}

	return next_year_state
}

func StringForPots(pots []Pot) string {
	output := []string{}
	for _, v := range pots {
		output = append(output, v.value)
	}

	return strings.Join(output, "")
}

func GetRules(file_location string) map[string]string {
	rules := make(map[string]string)

	file, err := os.Open(file_location)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	rules_reg := regexp.MustCompile(`(.....) => (.)`)
	for scanner.Scan() {
		rule_expression := rules_reg.FindStringSubmatch(scanner.Text())
		rules[rule_expression[1]] = rule_expression[2]
	}

	return rules
}

func CreatePots(initial_state string) []Pot {
	pots := []Pot{}
	for i, val := range strings.Split(initial_state, "") {
		pots = append(pots, Pot{position: i, value: val})
	}
	return pots
}
